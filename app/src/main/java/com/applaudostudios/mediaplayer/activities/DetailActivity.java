package com.applaudostudios.mediaplayer.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.applaudostudios.mediaplayer.R;

public class DetailActivity extends AppCompatActivity {

    public static final String EXT_NAME_RADIO = "EXT_NAME_RADIO";

    /**
     * This function creates a Intent with data gives
     *
     * @param context for create intent
     * @param name String to name the radio
     * @return intent with data gives
     */
    public static Intent getIntent(Context context,String name){
        Intent intent = new Intent(context,DetailActivity.class);
        if (name.equals("")){
            intent.putExtra(EXT_NAME_RADIO,name);
        }
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        if (getSupportActionBar()!= null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        TextView txvDetail = findViewById(R.id.txvDetailRadio);
        txvDetail.setText(R.string.detail_radio_info);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
