package com.applaudostudios.mediaplayer.adapters;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.applaudostudios.mediaplayer.R;

import java.util.ArrayList;
import java.util.List;

public class RadioAdapter extends RecyclerView.Adapter<RadioAdapter.RadioViewHolder> {

    private OnItemRadioClick mListener;
    private List<String> mRadios;

    /**
     * This function is the constructor
     *
     * @param callback
     */
    public RadioAdapter(OnItemRadioClick callback) {
        mListener = callback;
        mRadios = new ArrayList<>();
    }

    /**
     *
     * This function set data to mRadios and update data of the RecyclerView
     *
     * @param radios List<String> with the data.
     */
    public void setData(List<String> radios) {
        mRadios = radios;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RadioViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_radio, parent, false);
        return new RadioViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RadioViewHolder holder, int position) {
        holder.bind(mRadios.get(position));
    }

    @Override
    public int getItemCount() {
        return mRadios.size();
    }


    public class RadioViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView mRadioName;
        private ConstraintLayout lyItemRadio;

        public RadioViewHolder(View itemView) {
            super(itemView);
            mRadioName = itemView.findViewById(R.id.txvRadio);
            lyItemRadio = itemView.findViewById(R.id.lyItemRadio);
            lyItemRadio.setOnClickListener(this);
        }

        /**
         *This function set data to TextView
         *
         * @param nameRadio String with the name of the radio
         */
        private void bind(String nameRadio) {
            mRadioName.setText(nameRadio);
        }

        @Override
        public void onClick(View v) {
            mListener.onClickRadio(mRadios.get(getAdapterPosition()));
        }
    }

    /**
     * This interfaces is used with callBack to notify when was click in the item
     */
    public interface OnItemRadioClick {
        /**
         *take a parameter of type String
         *
         * @param radio parameter of type String
         */
        void onClickRadio(String radio);
    }
}
