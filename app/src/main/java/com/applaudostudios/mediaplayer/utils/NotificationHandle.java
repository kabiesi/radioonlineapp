package com.applaudostudios.mediaplayer.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.widget.RemoteViews;

import com.applaudostudios.mediaplayer.R;
import com.applaudostudios.mediaplayer.activities.MainActivity;
import com.applaudostudios.mediaplayer.services.PlayRadioService;

public class NotificationHandle {

    private static final int TYPE_ACTIVITY = 1;
    private static final int TYPE_SERVICE = 2;
    private static final int ACTION_MEDIA_PLAY = 1;
    private static final int ACTION_MEDIA_PAUSE = 2;
    private static final int ACTION_MEDIA_MUTE = 3;
    private static final int ACTION_MEDIA_NOT_MUTE = 4;
    public static final String EXT_RADIO_NAME = "EXT_RADIO_NAME";

    /**
     *This function creates a channel if android is Oreo.
     *
     * @param context for get getSysTemService
     * @param cha name of channel
     */
    public static void initChannel(Context context, String cha) {
        if (Build.VERSION.SDK_INT < 26) {
            return;
        }
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationChannel channel = new NotificationChannel(cha,
                cha,
                NotificationManager.IMPORTANCE_DEFAULT);
        channel.setDescription("Channel description");
        if (notificationManager != null) {
            notificationManager.createNotificationChannel(channel);
        }
    }

    /**
     *
     * This function creates a custom notification with the PendingIntents for its buttons
     * and a PendingIntent for the notification and a PendingIntent for when Notification
     * is deleted
     *
     * @param context for create the pending intents
     * @param subTitle for set a TextView of the subTitle
     * @param actionMedia this parameter is used for the conditions for take Visible or Invisible the buttons
     * @return a Object Notification
     */
    public static Notification showNotificationCustomRadio(Context context, String subTitle, int [] actionMedia) {
        RemoteViews views = new RemoteViews(context.getPackageName(),
                R.layout.notification_play_radio);

        RemoteViews viewsLarge = new RemoteViews(context.getPackageName(),
                R.layout.notification_play_radio_large);

        if (actionMedia[0] == ACTION_MEDIA_PLAY) {
            viewsLarge.setViewVisibility(R.id.imvActionPlay, View.INVISIBLE);
            viewsLarge.setViewVisibility(R.id.imvActionPause, View.VISIBLE);
        } else if (actionMedia[0] == ACTION_MEDIA_PAUSE) {
            viewsLarge.setViewVisibility(R.id.imvActionPause, View.INVISIBLE);
            viewsLarge.setViewVisibility(R.id.imvActionPlay, View.VISIBLE);
        }

        if (actionMedia[1] == ACTION_MEDIA_MUTE){
            viewsLarge.setViewVisibility(R.id.imvActionMute,View.VISIBLE);
            viewsLarge.setViewVisibility(R.id.imvActionNotMute,View.INVISIBLE);
        }else if (actionMedia[1] == ACTION_MEDIA_NOT_MUTE){
            viewsLarge.setViewVisibility(R.id.imvActionMute,View.INVISIBLE);
            viewsLarge.setViewVisibility(R.id.imvActionNotMute,View.VISIBLE);
        }

        viewsLarge.setOnClickPendingIntent(R.id.imvActionPlay,
                getPendingIntent(context, getIntent(context, PlayRadioService.PLAY_ACTION, null, PlayRadioService.class, 0),
                        TYPE_SERVICE, 0));
        viewsLarge.setOnClickPendingIntent(R.id.imvActionPause,
                getPendingIntent(context, getIntent(context, PlayRadioService.STOP_ACTION, null, PlayRadioService.class, 0),
                        TYPE_SERVICE, 0));
        viewsLarge.setTextViewText(R.id.txvSubTitle, subTitle);


        viewsLarge.setOnClickPendingIntent(R.id.imvActionMute,
                getPendingIntent(context,getIntent(context,PlayRadioService.MUTE_ACTION,null,PlayRadioService.class,0),
                        TYPE_SERVICE,0));

        viewsLarge.setOnClickPendingIntent(R.id.imvActionNotMute,
                getPendingIntent(context,getIntent(context,PlayRadioService.UNMUTE_ACTION,null,PlayRadioService.class,0),
                        TYPE_SERVICE,0));

        viewsLarge.setOnClickPendingIntent(R.id.imvCloseNotification,
                getPendingIntent(context,getIntent(context,PlayRadioService.CLOSE_ACTION,null,PlayRadioService.class, 0),
                        TYPE_SERVICE,0));

        Bitmap icon = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.radio_notification);

        initChannel(context, PlayRadioService.CHANNEL_LISTEN);

        return new NotificationCompat.Builder(context, PlayRadioService.CHANNEL_LISTEN).
                setContentTitle("Radio Music")
                .setTicker("Radio Music")
                .setContentText("Music")
                .setSmallIcon(R.drawable.radio_notification,2)
                .setLargeIcon(
                        Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setOngoing(true)
                .setCustomContentView(views)
                .setCustomBigContentView(viewsLarge)
                .setContentIntent(getPendingIntent(context,
                        getIntent(context, PlayRadioService.MAIN_ACTION, subTitle, MainActivity.class,
                                Intent.FLAG_ACTIVITY_SINGLE_TOP),
                        TYPE_ACTIVITY, PendingIntent.FLAG_UPDATE_CURRENT))
                .setDeleteIntent(getPendingIntent(context,
                        getIntent(context, PlayRadioService.DELETE_ACTION, null, PlayRadioService.class, 0), TYPE_SERVICE,
                        PendingIntent.FLAG_CANCEL_CURRENT))
                .build();
    }

    /**
     *
     * this function creates a pending intent for a intent in specific and a these type getService
     * with type TYPE_SERVICE and getActivity with type TYPE_ACTIVITY and a flag
     *
     * @param context for create PendingIntent
     * @param intent with the action a perform
     * @param type for make the condition
     * @param flag flag for pendingIntent
     * @return a PendingIntent with values gives
     */
    private static PendingIntent getPendingIntent(Context context, Intent intent, int type, int flag) {
        switch (type) {
            case TYPE_SERVICE:
                return PendingIntent.getService(context, 0, intent, flag);
            case TYPE_ACTIVITY:
                return PendingIntent.getActivity(context, 0, intent, flag);
        }
        return null;
    }

    /**
     *
     * This function create a intent with a action, a stringExtra and the flags
     * for a class that is receiver as parameter.
     *
     * @param context for create the intent
     * @param action for set this action in the intent
     * @param name String extra that is send in the intent
     * @param clazz class for create intent explicit
     * @param flags array the flag for intent
     * @return a intent with values gives.
     */
    public static Intent getIntent(Context context, String action, String name, Class clazz, int... flags) {
        Intent intent = new Intent(context, clazz);
        intent.setAction(action);
        if (name != null) {
            intent.putExtra(EXT_RADIO_NAME, name);
        }
        if (flags.length > 0) {
            for (int f : flags) {
                intent.setFlags(f);
            }
        }
        return intent;

    }
}
