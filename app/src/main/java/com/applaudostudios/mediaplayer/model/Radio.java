package com.applaudostudios.mediaplayer.model;

import java.util.ArrayList;
import java.util.List;

public class Radio {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * this function create a list with three radios
     *
     * @return a list of String with radios
     */
    public static List<String> getRadios() {
        List<String> radios = new ArrayList<>();
        radios.add("http://us5.internet-radio.com:8110/listen.pls&t=.m3u");
        radios.add("http://live.audiospace.co:80/energyfmtenerife");
        radios.add("http://media.dominiocreativo.com:8002/coolfm?type=.mp3");
        return radios;
    }
}
